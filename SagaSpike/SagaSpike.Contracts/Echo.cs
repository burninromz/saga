﻿using NServiceBus;

namespace SagaSpike.Backend.Contracts
{
    public class Echo : IMessage
    {
        public string SayId { get; set; }
        public string Message { get; set; }
        public string ClientConnectionId { get; set; }
    }
}