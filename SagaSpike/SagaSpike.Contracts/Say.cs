﻿using NServiceBus;
using NServiceBus.Saga;

namespace SagaSpike.Backend.Contracts
{
    public class Say :ICommand
    {
        public string Message { get; set; }
        public string SayId { get; set; }
        public string ClientConnectionId { get; set; }
    }
}
