﻿using System;
using NServiceBus.Saga;

namespace SagaSpike.Backend
{
    public class SayData : ISagaEntity
    {
        public string SayId { get; set; }
        public Guid Id { get; set; }
        public string Originator { get; set; }
        public string OriginalMessageId { get; set; }
    }
}
