﻿using System;
using NServiceBus.Saga;
using SagaSpike.Backend.Contracts;

namespace SagaSpike.Backend
{
    public class MySaga:Saga<SayData>,
                        IAmStartedByMessages<Say>,
                                         IHandleTimeouts<SayTimeout>
    {
        public override void ConfigureHowToFindSaga()
        {
            ConfigureMapping<Say>(s=>s.SayId,m=>m.SayId);
            base.ConfigureHowToFindSaga();
        }

        public void Handle(Say message)
        {
            Data.SayId = message.SayId;
            var echoMessage = new Echo
                                  {
                                      Message = String.Format("Echo : {0}", message.Message),
                                      ClientConnectionId = message.ClientConnectionId
                                  };
            ReplyToOriginator(echoMessage);
        }

        public void Timeout(SayTimeout state)
        {
            throw new NotImplementedException();
        }
    }

    public class SayTimeout
    {
    }
}