﻿$(function () {
    var svc = null;
    function SagaModel() {
        var self = this;

        self.Message = ko.observable("Something to say.");

        self.Responses = ko.observableArray();

        self.SendMessage = function () {
            svc.server.saySomething(self.Message());
        };

        self.dataBind = function () {
            ko.applyBindings(self);
        };

        self.dataRecieved = function (mess) {
            self.Responses.push(mess);
        };
    };
    var model = new SagaModel();
    model.dataBind();

    // ============================== Signal R Connection =================================================
    var timeOut;

    svc = $.connection.echoService;

    svc.client.saySomethingCompleted = function (message) {
        clearTimeout(timeOut);
        model.dataRecieved(message);
    };

    $.connection.hub.error(function (error) {
        window.alert('We are experiencing a technical issue. Please try later or contact us directly. Thank you.');
    });

    $.connection.hub.start().done(function () {
    }).fail(function () { alert("Connection failed!"); });

});
