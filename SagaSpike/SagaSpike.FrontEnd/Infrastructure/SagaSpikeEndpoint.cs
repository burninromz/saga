﻿

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using NServiceBus;
using SagaSpike.Backend.Contracts;

namespace SagaSpike.FrontEnd.Infrastructure
{
    [HubName("echoService")]
    public class SagaSpikeEndpoint : Hub, IHandleMessages<Echo>
    {
        private IHubContext CurrentContext
        {
            get { return GlobalHost.ConnectionManager.GetHubContext<SagaSpikeEndpoint>(); }
        }

        private IBus Bus
        {
            get
            {
                var bus = GlobalHost.DependencyResolver.Resolve<IBus>();
                return bus;
            }
        }

        private dynamic ClientConnection(string connectionId)
        {
            return CurrentContext.Clients.Client(connectionId);
        }

        public void SaySomething(string mess)
        {
            Bus.Send(new Say
                         {
                             SayId="30",
                             Message = mess,
                             ClientConnectionId = Context.ConnectionId
                         });
        }

        public void Handle(Echo message)
        {
            var client = ClientConnection(message.ClientConnectionId);
            if (client == null) return;
            client.SaySomethingCompleted(message.Message);
        }
    }
}